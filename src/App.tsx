import React, { useState, useEffect, useMemo } from "react";
import { ReactComponent as StartIcon } from "./assets/play.svg";
import "./App.css";

const chupas = require("./assets/chupas.opus");
const ya_we = require("./assets/yawe.mp3");

interface IRestTime {
  count: number;
  needRest: boolean;
}

const App: React.FC = () => {
  const [start, setStart] = useState<boolean>(false);
  const [minutes, setMinutes] = useState<number>(25);
  const [seconds, setSeconds] = useState<number>(0);
  const [restCount, setRestCount] = useState<IRestTime>({
    count: 0,
    needRest: false,
  });

  const chupas_audio = useMemo(() => new Audio(chupas.default), []);
  const ya_we_audio = useMemo(() => new Audio(ya_we.default), []);

  const startCount: React.MouseEventHandler<HTMLDivElement> = () => {
    setStart(true);
  };
  const pauseCount: React.MouseEventHandler<HTMLDivElement> = () => {
    setStart(false);
  };

  const seconds_str: string = seconds < 10 ? `0${seconds}` : `${seconds}`;
  const minutes_str: string = minutes < 10 ? `0${minutes}` : `${minutes}`;

  if (start) {
    setTimeout(() => {
      // Work time
      if (!restCount.needRest) {
        // Time reach zero and work time have finished
        if (!minutes && !seconds) {
          // Long break
          if (restCount.count === 3) {
            setMinutes(15);
            setSeconds(0);
            setRestCount({ needRest: true, count: 0 });
            // Short break
          } else {
            setMinutes(5);
            setSeconds(0);
            setRestCount({ needRest: true, count: restCount.count + 1 });
          }
          // Time doesn't reach zero, get back to work lazy man
        } else {
          // Discount of time
          if (!!seconds) {
            setSeconds(seconds - 1);
          } else {
            setSeconds(59);
            setMinutes(minutes - 1);
          }
        }
        // Break time
      } else {
        // Time reach zero and break time have finished
        if (!minutes && !seconds) {
          setMinutes(25);
          setSeconds(0);
          setRestCount({ ...restCount, needRest: false });
          // Time doesn't reach zero and break time continues
        } else {
          // Discount of time
          if (!!seconds) {
            setSeconds(seconds - 1);
          } else {
            setSeconds(59);
            setMinutes(minutes - 1);
          }
        }
      }
    }, 1000);
  }

  useEffect(() => {
    if (restCount.needRest) {
      ya_we_audio.play();
    } else {
      chupas_audio.play();
    }
  }, [ya_we_audio, chupas_audio, restCount]);

  return (
    <div className="app">
      <h1>POMODORO TIMER</h1>
      {start ? (
        <>
          <div
            className="timer"
            onClick={pauseCount}
          >{`${minutes_str}:${seconds_str}`}</div>
          Click time to pause
        </>
      ) : (
        <div className="button-start" onClick={startCount}>
          <StartIcon />
        </div>
      )}

      {start && <h1>{restCount.needRest ? "TAKE A BREAK" : "TIME TO WORK"}</h1>}
    </div>
  );
};

export default App;
